<?php

namespace app\Constant;
class Constant
{
    const IS_LOCKED = 1;
    const UN_LOCKED = 0;
    const MIN_ATTEMPT_COUNT = 0;
    const MAX_ATTEMPT_COUNT = 3;
    const FIFTH_LEVEL_ID = 5;
    const OFFICE_CHECK_TASK_STATUS = 3;
    const SELF_CHECK_TASK_STATUS = 2;

}