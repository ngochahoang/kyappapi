<?php


namespace app\controllers;

use app\models\ResetPasswordForm;
use app\models\SettingPasswordForm;
use app\models\User;
use Yii;

use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

class UserController extends Controller
{
    public $layout = 'main-login';

    /**
     * @return array|string
     */
    public function actionResetPassword(){
        $model = new ResetPasswordForm;
        if($model->load(Yii::$app->request->post()) &&  $model->validate()){
            if($model->validateUser()){
                Yii::$app->session->setFlash('success','Success');
            }
        }
        return $this->render('reset-password',['model' =>$model]);
    }


    public function actionTest(){
        $pwd = 'AAAAFT1000020?$';
        $password = str_split($pwd);
        $isNumber = $isLowerCase = $isUpperCase = $isSpecialCharacter = $hasUserCode = false;

        $userCode = 'FT1000020';

        if(stripos($pwd,$userCode)){
            $hasUserCode = true;
        }
        if(!$hasUserCode){
            $check = 0;
            foreach ($password as $character){
                if(ctype_digit($character)){
                    $isNumber = true;
                }
                else if(ctype_upper($character)){
                    $isUpperCase = true;
                }
                else if(ctype_lower($character)){
                    $isLowerCase = true;
                }
                else{
                    $isSpecialCharacter = true;
                }
                $check = $isNumber + $isUpperCase + $isLowerCase + $isSpecialCharacter;
                if($check >= 2){
                    break;
                }

            }
            if($check >= 2){
                echo 'Valid Password';
            }else{
               echo 'Invalid password';
            }
        }else{
            echo 'Password doesn\'t include userCode';
        }


    }

    public function actionTest1($userCode){
        echo $userCode;
    }




}